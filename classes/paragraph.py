import json

from db import DbObject


class Paragraph(DbObject):
    _db_table_name = "paragraphs"

    def __init__(self, id, paragraph_text, response_json, article_id):
        super().__init__()

        self._response_json_object = None
        self.id = id
        self.paragraph_text = paragraph_text
        self.response_json = response_json
        self.article_id = article_id
        self.response_json = None

    @staticmethod
    def getParagraphById(id):
        # Make separate connections to support threading
        db = DbObject.connect()
        cursor = db.cursor()

        query = """SELECT * FROM paragraphs WHERE id=? LIMIT 1"""
        cursor.execute(query, (id,))
        # We either retrieve one element from result or raise exception
        for (id, paragraph_text, response_json, article_id) in cursor:
            return Paragraph(id, paragraph_text, response_json, article_id)
        raise("Paragraph with id {} not found!".format(id))

