import json

from db import DbObject


class Relation(DbObject):
    _db_table_name = "relations"

    def __init__(self, raw_relation):
        super().__init__()

        self.raw_relation = raw_relation
