import json

from db import DbObject


class RelationExample(DbObject):
    _db_table_name = "relation_examples"

    def __init__(self, id, subj, raw_relation, obj, relation_id, sentence_id, paragraph_id, article_id, count):
        super().__init__()

        self.id = id
        self.subj = subj
        self.raw_relation = raw_relation
        self.obj = obj
        self.relation_id = relation_id
        self.sentence_id = sentence_id
        self.paragraph_id = paragraph_id
        self.article_id = article_id
        self.count = count

    @staticmethod
    def getAllRelations(limit):
        # Make separate connections to support threading
        db = DbObject.connect()
        cursor = db.cursor()

        query = """SELECT *, COUNT(relation_id) as count FROM relation_examples
                    GROUP BY relation_id
                    ORDER BY count desc
                    LIMIT {limit}""".format(limit=limit)
        cursor.execute(query, ())
        result = list()
        for (id, subj, raw_relation, obj, relation_id, sentence_id, paragraph_id, article_id, count) in cursor:
            result.append(RelationExample(id, subj, raw_relation, obj, relation_id, sentence_id, paragraph_id, article_id, count))
        return result


    @staticmethod
    def getRelationById(id):
        # Make separate connections to support threading
        db = DbObject.connect()
        cursor = db.cursor()

        query = """SELECT * FROM relation_examples WHERE id=? LIMIT 1"""
        cursor.execute(query, (id,))
        # We either retrieve one element from result or raise exception
        for (id, subj, raw_relation, obj, relation_id, sentence_id, paragraph_id, article_id) in cursor:
            return RelationExample(id, subj, raw_relation, obj, relation_id, sentence_id, paragraph_id, article_id, 1)
        raise("Relation with id {} not found!".format(id))



    @staticmethod
    def filterRelations(filter_name, filter, limit):
        # Make separate connections to support threading
        db = DbObject.connect()
        cursor = db.cursor()

        query = """SELECT * FROM relation_examples 
                    WHERE {filter_name} = ?
                    LIMIT {limit}""".format(limit=limit, filter_name=filter_name)

        cursor.execute(query, (filter,))
        result = list()
        for (id, subj, raw_relation, obj, relation_id, sentence_id, paragraph_id, article_id) in cursor:
            result.append(RelationExample(id, subj, raw_relation, obj, relation_id, sentence_id, paragraph_id, article_id, 1))
        return result