import json
from db import DbObject


class Article(DbObject):
    _db_table_name = "articles"

    def __init__(self, id, json_data):
        super().__init__()

        self.id = id
        self.article_object_json = json.loads(json_data)
        self.content = self.article_object_json["content"]
        self._json_object = None

    def content(self):
        if self._json_object is None:
            self._json_object = json.loads(self.article_object_json)

        return self._json_object["content"]

    def __html__(self):
        # Chameleon treats this as XML content
        return self.content


    @staticmethod
    def getArticleById(id):
        # Make separate connections to support threading
        db = DbObject.connect()
        cursor = db.cursor()

        query = """SELECT * FROM articles WHERE id=? LIMIT 1"""
        cursor.execute(query, (id,))
        # We either retrieve one element from result or raise exception
        for (id, json) in cursor:
            return Article(id, json)
        raise("Paragraph with id {} not found!".format(id))

