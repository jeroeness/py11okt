import os
import time
from config import cfg
from db import connect

db_file_name = cfg['database']['sqlite_file']
os.rename(db_file_name, db_file_name + str(time.time()))

db = connect()


def _init_database():
    cursor = db.cursor()

    ###
    # Articles
    #
    # Articles are taken from the scraped jsonl file. The full json is saved here and referenced by id.
    # After parsing, the article is split into 'paragraphs' which are stored in a separate table.
    ###
    cursor.execute('''
        CREATE TABLE articles(
            id INTEGER PRIMARY KEY,
            article_object_json TEXT NOT NULL)
    ''')

    ###
    # Paragraphs
    #
    # An article is parsed (html) and this yields multiple parts of flat text. These paragraphs become queries,
    # as these are sent to the NLP pipeline as such.
    #
    # paragraph text:       The data that is yielded from parsing the article html and sent to the pipeline.
    # Response json:        This is the raw data that is returned from the NLP pipeline. It contains all the sentences,
    #                       among other (meta)data.
    # Article id:           The paragraph comes from an article, which we reference for traceability.
    cursor.execute('''
        CREATE TABLE paragraphs(
            id INTEGER PRIMARY KEY,
            paragraph_text TEXT NOT NULL,
            response_json TEXT,
            article_id INTEGER NOT NULL,

            FOREIGN KEY (article_id) REFERENCES articles(id))
    ''')

    ###
    # Relation groups
    #
    # A group 'manually' assigned to relations to group them together.
    ###
    cursor.execute('''
        CREATE TABLE relation_groups(
            id INTEGER PRIMARY KEY,
            relation_group_name TEXT)
    ''')

    ###
    # Relations
    #
    # The set of all unique relations that we have extracted from the articles.
    #
    # Raw relation:         The relation as it is returned from the pipeline verbatim. Case sensitive.
    # Relation root:        A simplified version of the relation so as to relate certain similar relations
    #                       automatically. This should always be lowercase.
    # Relation group:       A group 'manually' assigned to relations to group them together.
    ###
    cursor.execute('''
        CREATE TABLE relations(
            id INTEGER PRIMARY KEY,
            raw_relation TEXT NOT NULL,
            relation_root TEXT,
            relation_group_id INTEGER)
    ''')

    ###
    # Relation examples
    #
    # Every time a relation is detected in the article by the pipeline, it becomes a relation example.
    #
    # Subj:                 The subject of the extracted relation example
    # Raw relation:         The relation as it is returned from the pipeline verbatim.
    # Obj:                  The object of the extracted relation example
    # Relation id:          Reference to our unique relation ID
    # Sentence id:          The key of the sentence in the paragraph response (see paragraphs table)
    # paragraph id:         The NLP paragraph that yielded this example
    # Article id:           The article in which this relation was found
    ###
    cursor.execute('''
        CREATE TABLE relation_examples(
            id INTEGER PRIMARY KEY,
            subj TEXT NOT NULL,
            raw_relation TEXT NOT NULL, 
            obj TEXT NOT NULL, 
            relation_id INTEGER NOT NULL,
            sentence_id INTEGER NOT NULL,
            paragraph_id INTEGER NOT NULL,
            article_id INTEGER NOT NULL,

            FOREIGN KEY (relation_id) REFERENCES relations(id),
            FOREIGN KEY (paragraph_id) REFERENCES paragraphs(id),
            FOREIGN KEY (article_id) REFERENCES articles(id))
    ''')

    db.commit()


_init_database()
db.close()
