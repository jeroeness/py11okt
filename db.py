from config import cfg
import sqlite3



class DbObject:
    _db_table_name = None

    def __init__(self):
        self.id = None

    @staticmethod
    def connect():
        return sqlite3.connect(cfg['database']['sqlite_file'])

    def insert(self):

        if self._db_table_name is None:
            raise NotImplementedError("Table name not specified for this class!")

        query = "INSERT INTO {table} ".format(table=self._db_table_name)

        columns, values = self._get_db_data()

        query += "({columns}) VALUES ({values})".format(columns=",".join(columns),
                                                        values=",".join(['?'] * len(values)))

        # We make separate connections because of threading
        db = self.connect()
        cursor = db.cursor()
        cursor.execute(query, tuple(values))

        inserted_id = cursor.lastrowid
        db.commit()
        db.close()

        self.id = inserted_id

        return inserted_id

    def update(self):

        if self._db_table_name is None:
            raise NotImplementedError("Table name not specified for this class!")

        if self.id is None:
            raise ValueError("This object has no ID!")

        query = "UPDATE {table} SET ".format(table=self._db_table_name)

        columns, values = self._get_db_data()

        for i in range(len(columns)):
            column = columns[i]

            query += "{column}=?, ".format(column=column)

        comma_fix = query.rsplit(', ', 1)
        query = comma_fix[0] + " WHERE id={id}".format(id=self.id)

        # We make separate connections because of threading
        db = self.connect()
        cursor = db.cursor()
        cursor.execute(query, tuple(values))
        db.commit()
        db.close()



    def _get_db_data(self):
        columns, values = [], []
        for column, value in self.__dict__.items():
            if column[0] == '_':
                continue
            if column == 'id':
                continue

            columns.append(column)
            values.append(value)
        return columns, values
