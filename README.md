Relation explorer
=================
Enables the user to 'explore' extracted relations in the database.

11 Oktober -- Jeroen Esseveld

Here are some developer notes.

 - Chameleon is used as template engine, see Chameleon docs: https://chameleon.readthedocs.io/en/latest/index.html
 - The Assignment states that the code should work efficiently with massive databases. Unforntunately SQLite is not build to work efficient with big databases, SQL optimizations are limited.
 - Bootstrap is used as stylesheet
 - for the implementation I did not have to make any changes to the database and I did not require extra classes for Object and Subject. This saved some time.
 
 
 
 ## running
 
In a bash terminal:
 ```bash
 source venv/bin/activate
 python3 relation-explorer.py
  ```
Go to [http://127.0.0.1:8912](http://127.0.0.1:8912)