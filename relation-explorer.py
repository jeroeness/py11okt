import json
import sys
import operator
import os

from classes.article import Article
from classes.paragraph import Paragraph
from http.server import BaseHTTPRequestHandler, HTTPServer
from urllib.parse import urlparse, parse_qs
from classes.relation_example import RelationExample
from chameleon import PageTemplateLoader

"""
Very simple HTTP server in python.
Usage::
    ./dummy-web-server.py [<port>]
Send a GET request::
    curl http://localhost
Send a HEAD request::
    curl -I http://localhost
Send a POST request::
    curl -d "foo=bar&bin=baz" http://localhost
"""
relations_file = 'rel-new.txt'
http_port = 8912


class RelationsExplorerServerHandler(BaseHTTPRequestHandler):
    def __init__(self, *args, **kwargs):

        path = os.path.dirname(__file__)
        self.templates = PageTemplateLoader(os.path.join(path, "templates"))


        super().__init__(*args, **kwargs)

        # GET query parsed
        self.query_components = None

    def _set_headers(self):
        self.send_response(200)
        self.send_header('Content-type', 'text/html')
        self.end_headers()

    def pagination(self):
        limit = "0, 100"
        if 'limit' in self.query_components:
            limit = self.query_components["limit"][0]

        split = limit.split(',')
        if len(split) == 1:
            split.append(split[0])
        offset = int(split[0])
        size = int(split[1])
        limits = {
            "limit": limit,
            "nextPage": ','.join([str(offset + size), str(size)]),
            "prevPage": ','.join([str(max(offset - size, 0)), str(size)])
        }
        self.limits=limits

    def do_GET(self):
        self._set_headers()

        self.query_components = parse_qs(urlparse(self.path).query)
        self.pagination()

        if 'relation' in self.query_components:
            response = self._get_relations("relation_id", "relation", self.query_components["relation"][0])
        elif 'object' in self.query_components:
            response = self._get_relations("obj", "object", self.query_components["object"][0])
        elif 'subject' in self.query_components:
            response = self._get_relations("subj", "subject", self.query_components["subject"][0])
        elif 'id' in self.query_components:
            response = self._get_single_relation(self.query_components["id"][0])
        else:
            response = self._index_relations()

        self.wfile.write(bytes(response, "utf-8"))

    def do_HEAD(self):
        self._set_headers()

    def do_POST(self):
        # Doesn't do anything with posted data
        content_length = int(self.headers['Content-Length'])  # <--- Gets the size of data
        post_data = self.rfile.read(content_length)  # <--- Gets the data itself
        self._set_headers()
        response = "<html><body><h1>POST!</h1></body></html>"
        self.wfile.write(bytes(response, "utf-8"))

    # dispaly a single relation by ID
    def _get_single_relation(self, id):
        try:
            relation = RelationExample.getRelationById(id)
            paragraph = Paragraph.getParagraphById(relation.paragraph_id)
            article = Article.getArticleById(relation.article_id)
            template = self.templates["relation.pt"]
            response = template(rel=relation,par=paragraph, article=article)
        except Exception as ex:
            response = " ".join(ex.args)
            print(ex)
        return response

    # Lists examples of the specified relation
    def _get_relations(self, filter, filter_url_param, value):
        try:
            relations = RelationExample.filterRelations(filter, value, self.limits["limit"])
            template = self.templates["relations.pt"]
            response = template(relationlist=relations, limit=self.limits, filter_url_param=filter_url_param, filter_value=value)
        except Exception as ex:
            response = " ".join(ex.args)
            print(ex)
        return response

    # List all unique relations
    def _index_relations(self):
        try:
            relations = RelationExample.getAllRelations(self.limits["limit"])
            template = self.templates["index.pt"]
            response = template(relationlist=relations, nextPage=self.limits["nextPage"], prevPage=self.limits["prevPage"])
        except Exception as ex:
            response = " ".join(ex.args)
            print(ex)
        return response

def run(server_class=HTTPServer, handler_class=RelationsExplorerServerHandler, port=80):
    server_address = ('', port)
    httpd = server_class(server_address, handler_class)
    print
    'Starting httpd...'
    httpd.serve_forever()

# Run the server
run(port=http_port)
